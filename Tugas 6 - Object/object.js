// Soal No. 1 (Array to Object)
console.log('Soal No. 1 (Array to Object)');

const arrayToObject = (array = []) => {
    const yearNow = new Date().getFullYear();
    if(array.length === 0) return "";
    return array.map((data, _) => {
        let age = '';
        if(yearNow > data[3]) age = yearNow - data[3];
        else age = 'Invalid Date'
        return {
            firstName:data[0],
            lastName:data[1],
            gender:data[2],
            age: age,
        }
    });
}

console.log(arrayToObject(
    [
        ["Abduh", "Muhamad", "male", 1992], 
        ["Ahmad", "Taufik", "male", 1985]
    ]
));
/**
 * 1. Abduh Muhamad : { firstName: "Abduh", lastName: "Muhamad", gender: "male", age: 28}
 * 2. Ahmad Taufik : { firstName: "Ahmad", lastName: "Taufik", gender: "male", age: 35} 
 */
// Driver Code
var people = [
     ["Bruce", "Banner", "male", 1975], 
     ["Natasha", "Romanoff", "female"] 
]
console.log(arrayToObject(people) )
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ 
    ["Tony", "Stark", "male", 1980], 
    ["Pepper", "Pots", "female", 2023] 
]
console.log(arrayToObject(people2) )
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
console.log(arrayToObject([])) // ""
console.log()


console.log('Soal No. 2 (Shopping Time)');

const shoppingTime = (memberId = '', money = 0) => {
    // you can only write your code here!
    const items = [{
        'name': 'Sepatu brand Stacattu',
        'price': 1500000,
    }, {
        'name': 'Baju brand Zoro',
        'price': 500000,
    }, {
        'name': 'Baju brand H&N',
        'price': 250000,
    }, {
        'name': 'Sweater brand Uniklooh',
        'price': 175000,
    }, {
        'name': 'Casing Handphone',
        'price': 50000,
    }]

    if ( memberId === '' ) return 'Mohon maaf, toko X hanya berlaku untuk member saja';
    else if ( money === 0 ) return 'Mohon maaf, uang tidak cukup';

    const listPurchased = [];
    let changeMoney = money;
    let countMoneyNotEnough = 0;
    for(const item of items) {
        if((money - item.price) > 0) {
            listPurchased.push(item.name);
            changeMoney -= item.price;
        } else {
            countMoneyNotEnough++;
        }

        if(listPurchased.length === 0 && countMoneyNotEnough === items.length) return 'Mohon maaf, uang tidak cukup';
    }

    return {
        memberId: memberId,
        money: money,
        listPurchased: listPurchased,
        changeMoney: changeMoney,
    }
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
console.log();

console.log('Soal No. 3 (Naik Angkot)');

function naikAngkot(arrPenumpang = []) {
    const rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    return arrPenumpang.map((dataPenumpang, _) => {
        let bayar = 0;
        for(const r of rute) {
            if( r > dataPenumpang[1] && r <= dataPenumpang[2] ) {
                bayar += 2000;
            }
        }
        return {
            penumpang: dataPenumpang[0],
            naikDari: dataPenumpang[1],
            tujuan: dataPenumpang[2],
            bayar: bayar,
        };
    })

}

//TEST CASE
console.log(naikAngkot([
    ['Dimitri', 'B', 'F'], 
    ['Icha', 'A', 'B']
]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]
