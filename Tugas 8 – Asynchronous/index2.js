console.log('Soal No. 2 (Promise Baca Buku)');

var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
readBooksPromise(
    10000,
    books[0]
).then(data => {
    readBooksPromise(
        data,
        books[1]
    ).then(
        data => {
            readBooksPromise(
                data,
                books[2]
            )
            .then()
            .catch(err => {
                console.log(err)
            })
        }
    ).catch(err => {
        console.log(err)
    })
}).catch(err => {
    console.log(err)
})
