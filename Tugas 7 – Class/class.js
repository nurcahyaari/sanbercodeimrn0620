console.log('1. Animal Class');
console.log('Release 0');
class Animal {
    // Code class di sini
    constructor(
        name,
    ) {
        this.name = name;
        this.legs = 4;
        this.cold_blooded = false;
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
console.log()
console.log('Release 1');

// Code class Ape dan class Frog di sini
class Ape extends Animal {
    constructor(name) {
        super(name);
        super.legs = 2;
    }

    yell() {
        console.log('Auooo');
    }
} 

class Frog extends Animal {
    constructor(name) {
        super(name);
    }

    jump() {
        console.log('hop hop');
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

console.log()

console.log('2. Function to Class');
// function Clock({ template }) {
  
//   var timer;

//   function render() {
//     var date = new Date();

//     var hours = date.getHours();
//     if (hours < 10) hours = '0' + hours;

//     var mins = date.getMinutes();
//     if (mins < 10) mins = '0' + mins;

//     var secs = date.getSeconds();
//     if (secs < 10) secs = '0' + secs;

//     var output = template
//       .replace('h', hours)
//       .replace('m', mins)
//       .replace('s', secs);

//     console.log(output);
//   }

//   this.stop = function() {
//     clearInterval(timer);
//   };

//   this.start = function() {
//     render();
//     timer = setInterval(render, 1000);
//   };

// }

// var clock = new Clock({template: 'h:m:s'});
// clock.start(); 

class Clock {
    // Code di sini
    constructor({template}) {
        this.timer;
        this.template = template;
        this.render = this.render.bind(this);
    }

    render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
        this.output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

        console.log(this.output);
    }

    stop() {
        clearInterval(this.timer);
    };

    start() {
        this.timer = setInterval(this.render, 1000);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  
setTimeout(() => {
    clock.stop();
}, 5000);
