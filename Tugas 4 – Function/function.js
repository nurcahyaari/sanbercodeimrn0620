/*
** No. 1 
** Tulislah sebuah function dengan nama teriak() 
** yang mengembalikan nilai “Halo Sanbers!” 
** yang kemudian dapat ditampilkan di console.
*/
console.log('Soal No. 1');
const teriak = () => console.log("Hallo Sanbers!");
teriak();
console.log();

/*
** No. 2 
** Tulislah sebuah function dengan nama kalikan() 
** yang mengembalikan hasil perkalian dua parameter yang di kirim.
**
*/
console.log('Soal No. 2');
const kalikan = (...args) => args.reduce((v, d) => v * d);
const val = [12, 4];
console.log(`Hasil Perkalian ${val} = ${kalikan(...val)}`);
console.log();

/*
** No. 3 
** Tulislah sebuah function dengan nama introduce() 
** yang memproses paramater yang dikirim menjadi 
** sebuah kalimat perkenalan seperti berikut: 
** “Nama saya [Name], umur saya [Age] tahun, 
** alamat saya di [Address], dan saya punya hobby yaitu [hobby]!”
*/
console.log('Soal No. 3');
const introduce = (name, age, address, hobby) => `Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}!`;
console.log(introduce(
    'Ari',
    '21',
    'Semaki',
    'Tidur',
));
