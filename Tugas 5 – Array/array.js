console.log('Soal No. 1 (Range)');

// fungsi range
const range = (startNum = 0, finishNum = 0) => {
    if (
        startNum === 0 ||
        finishNum === 0
    ) {
        return -1;
    }
    if (startNum >= finishNum) {
        return [...Array(startNum + 1).keys()].filter((v, _) => v >= finishNum).reverse();
    } else {
        return [...Array(finishNum + 1).keys()].filter((v, _) => v >= startNum);
    }
}

console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11,18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range());// -1 
console.log();

console.log('Soal No. 2 (Range with Step)');
const rangeWithStep = (startNum = 0, finishNum = 0, step = 1) => {
    if (
        startNum === 0 ||
        finishNum === 0
    ) {
        return -1;
    }

    let array = [];
    if (startNum >= finishNum) {
        array = [...Array(startNum + 1).keys()].filter((v, _) => (v >= finishNum)).reverse()
        const returnedArray = [];
        for(let i = array[0]; i >= array[array.length - 1]; i-=step) {
            returnedArray.push(i);
        }
        return returnedArray;
    } else {
        array = [...Array(finishNum + 1).keys()].filter((v, _) => v >= startNum);
        const returnedArray = [];
        for(let i = startNum; i <= array[array.length - 1]; i+=step) {
            returnedArray.push(i);
        }
        return returnedArray;
    }
}

console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5] 
console.log();


console.log('Soal No. 3 (Sum of Range)');

const sum = (startNum = 0, finishNum = 0, step = 1) => {
    if (startNum !== 0 && finishNum === 0) return startNum;
    else if(startNum === 0 || finishNum === 0) return 0;

    let array = [];
    if (startNum >= finishNum) {
        array = [...Array(startNum + 1).keys()].filter((v, _) => (v >= finishNum)).reverse()
        const returnedArray = [];
        for(let i = array[0]; i >= array[array.length - 1]; i-=step) {
            returnedArray.push(i);
        }
        return returnedArray.reduce((v, d) => v + d);
    } else {
        array = [...Array(finishNum + 1).keys()].filter((v, _) => v >= startNum);
        const returnedArray = [];
        for(let i = startNum; i <= array[array.length - 1]; i+=step) {
            returnedArray.push(i);
        }

        return returnedArray.reduce((v, d) => v + d);
    }
}

// Code di sini
console.log(sum(1,10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15,10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0 
console.log();


console.log('Soal No. 4 (Array Multidimensi)');

const dataHandling = (data = []) => {
    let text = '';
    for(const d of data) {
        text += `Nomor ID:  ${d[0]}\nNama Lengkap:  ${d[1]}\nTTL:  ${d[2]} ${d[3]}\nHobi:  ${d[4]}\n\n`;
    }
    return text;
};

console.log(dataHandling([
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]));
console.log();


console.log('Soal No. 5 (Balik Kata)');

const balikKata = (text = '') =>  text.split('').reverse().join('');

console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I 
console.log();

console.log('Soal No. 6 (Metode Array)');
const dataHandling2 = (data) => {
    const newData = [data[0], `${data[1]}Elsharawy`, `Provinsi ${data[2]}`, data[3], 'Pria', 'SMA Internasional Metro'];
    const tanggalLahir = data[3];
    const tanggalSplited = tanggalLahir.split('/');
    let bulan = '';
    switch(parseInt(tanggalLahir.split('/')[1], 10)){
        case 1:{
            bulan = 'Januari';
            break;
        }
        case 2:{
            bulan = 'Februari';
            break;
        }
        case 3:{
            bulan = 'Maret';
            break;
        }
        case 4:{
            bulan = 'April';
            break;
        }
        case 5:{
            bulan = 'Mei';
            break;
        }
        case 6:{
            bulan = 'June';
            break;
        }
        case 7:{
            bulan = 'Juli';
            break;
        }
        case 8:{
            bulan = 'Agustus';
            break;
        }
        case 9:{
            bulan = 'September';
            break;
        }
        case 10:{
            bulan = 'Oktober';
            break;
        }
        case 11:{
            bulan = 'November';
            break;
        }
        case 12:{
            bulan = 'Desember';
            break;
        }
    }
    return [
        newData,
        bulan,
        [tanggalSplited[2], tanggalSplited[0], tanggalSplited[1]],
        tanggalSplited.join('-'),
        data[1],
    ];
}

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 
console.log(dataHandling2(["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"]));
