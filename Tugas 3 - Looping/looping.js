console.log('No. 1 Looping While');
console.log('LOOPING PERTAMA');
let i = 1;
while(i <= 20) {
    if(i % 2 === 0) {
        console.log(`${i} - I love coding`);
    }
    i++;
}

console.log('LOOPING KEDUA');
while(i >= 1) {
    if(i % 2 === 0) {
        console.log(`${i} - I will become a mobile developer`);
    }
    i--;
}
console.log();

console.log('No. 2 Looping menggunakan for');
// looping dengan for loop
for(let i = 1; i <= 20; i++) {
    if(i % 3 === 0 && i % 2 !== 0) {
        console.log(`${i} - I Love Coding `);
    } else if(i % 2 === 0) {
        console.log(`${i} - Berkualitas`);
    } else {
        console.log(`${i} - Santai`);
    }
}
console.log();


// No. 3 Membuat Persegi Panjang #
console.log('No. 3 Membuat Persegi Panjang #');
for(let i = 0; i < 4; i++) {
    for(let j = 0; j < 8; j++) {
        process.stdout.write('#');
    }
    console.log()
}
console.log();

// No. 4 Membuat Tangga
console.log('No. 4 Membuat Tangga');
for(let i = 0; i < 7; i++) {
    for(let j = 0; j <= i; j++) {
        process.stdout.write('#');
    }
    console.log();
}
console.log();


// No. 5 Membuat Papan Catur
console.log('No. 5 Membuat Papan Catur');
for(let i = 0; i < 8; i++) {
    for(let j = 0; j < 8; j++) {
        if(i % 2 === 0 && j % 2 !== 0) {
            process.stdout.write(' ');
            process.stdout.write('#');
        } else if(i % 2 !== 0 && j % 2 === 0){
            process.stdout.write('#');
            process.stdout.write(' ');
        }
    }
    console.log();
}
